
import { Component, ViewChild, OnInit } from "@angular/core";
import { MemberService, Member } from "app/member.service";
import { ActivatedRoute } from "@angular/router";
import { AuthService } from "app/auth.service";
import { DatePipe } from '@angular/common';
import { TournamentService, Tournament } from "app/tournament.service";
import { MyTableComponent, ColumnDef } from "app/mytable.component";
import { GameService, Game } from "app/game.service";
import { Observable } from "rxjs/Observable";
import { EditMemberComponent } from "app/edit-member.component";
import * as _ from 'lodash';
import * as moment from 'moment'



@Component({
    selector: 'espaceMember',
    templateUrl: './espaceMember.component.html',
    styleUrls:['./tournamentlist.component.css'],
})
export class EspaceMember {
    nbrNotAssigend: number;
    member: Member;
    gamesOfTournament: Game[];
    message: string;
    selectedTournamentAssigned: Tournament;
    selectedTournamentNotAssigned: Tournament;
    selectedGame: Game;



    @ViewChild('tournamentsNotAssigned') tournamentsNotAssigned: MyTableComponent;
    @ViewChild('tournamentsAssigned') tournamentsAssigned: MyTableComponent;
    @ViewChild('games') games: MyTableComponent;
    @ViewChild('editMember') editMember: EditMemberComponent;

    columnDefsTournamentsNotAssigned: ColumnDef[] = [
        { name: 'name', type: 'String', header: 'name', width: 1, filter: true, sort: 'asc' },
        { name: 'start', type: 'Date', header: 'start', width: 2, filter: true },
        { name: 'finish', type: 'Date', header: 'finish', width: 1, filter: true, align: 'center' },
        { name: 'maxPlayers', type: 'string', header: 'maxPlayers', width: 1, filter: true, align: 'center' },
    ];
    columnDefsTournamentsAssigned: ColumnDef[] = [
        { name: 'name', type: 'String', header: 'name', width: 1, filter: true, sort: 'asc' },
        { name: 'start', type: 'Date', header: 'start', width: 2, filter: true },
        { name: 'finish', type: 'Date', header: 'finish', width: 1, filter: true, align: 'center' },
        { name: 'maxPlayers', type: 'string', header: 'maxPlayers', width: 1, filter: true, align: 'center' },
    ];
    columnDefsGames: ColumnDef[] = [
        { name: 'playerOne', type: 'string', header: 'playerOne', width: 1, filter: true, sort: 'asc' },
        { name: 'scoreOne', type: 'number', header: 'score', width: 1, filter: true },
        { name: 'tournament', type: 'string', header: 'tournament', width: 1, filter: true, align: 'center' },
        { name: 'scoreTwoo', type: 'number', header: 'scoreTwoo', width: 1, filter: true, align: 'center' },
        { name: 'playerTwoo', type: 'string', header: 'playerTwoo', width: 1, filter: true, align: 'center' },
    ];

    constructor(public authService: AuthService, private memberService: MemberService,
        private tournamentService: TournamentService, private gameService: GameService) {
        this.memberService.getOne(this.authService.currentUser).subscribe(res => { this.member = res; });
    }

    public selectedItemTournamentNotAssigned(item) {
        this.selectedTournamentNotAssigned = this.tournamentsNotAssigned.selectedItem as Tournament;
        this.memberService.getNbrNotAssigned(this.selectedTournamentNotAssigned).subscribe(res => {this.nbrNotAssigend = res});
        this.message='';
    }
    public selectedItemTournamentAssigned(item) {
        this.selectedTournamentAssigned = this.tournamentsAssigned.selectedItem as Tournament;
        if(this.games)
        this.games.refresh();
        this.message='';
    }

    get getDataTournamentsNotAssignedService() {
        return m => this.tournamentService.getTournamentsNotAssigned(this.member);
    }

    get getDataTournamentsAssignedService() {
        return m => this.tournamentService.getTournamentsAssigned(this.member);
    }

    public selectedItemGame(item) {
        this.selectedGame = this.games.selectedItem as Game;
        this.message='';
    }

    get getDataGameService() {

        return g => this.gameService.getGame(this.member);
    }

    get updateGameService() {
        return g => this.gameService.update(g);
    }

    addAllTournamentsNotAssigned() {
        this.tournamentService.addAlltournaments(this.member).subscribe(res => {
            this.refreshVue(res);
            this.tournamentService.getAll().subscribe(ts=>{
                if(ts.length>res.tournaments.length)
                this.message="la date de ce tournois est déja passé ou complet, veuillez contaster l'administrator !";
            })
                
        })
    }

    addTournamentNotAssigned() {
        if (this.selectedTournamentNotAssigned) {
            if (this.verificationDateIscription()) {
                if (this.placeRestante() >= 1) {
                    this.tournamentService.addOneTournament(this.member, this.selectedTournamentNotAssigned)
                        .subscribe(res => this.refreshVue(res));
                }
                else
                    this.message = "  ce tournois est déjà complet veuillez contacter l'adminstrator ! " 
            }
            else
                this.message = "la date est expirée veuillez choisir un autre tournoi !"

        }
        else
            this.message = "veillez selectionner un Tournament!!"

    }

    removeTournamentAssigned() {
        if (this.selectedTournamentAssigned) {
            this.tournamentService.removeTournamentAssigned(this.member, this.selectedTournamentAssigned)
                .subscribe(res => this.refreshVue(res))
        }
    }
    removeAllTournamentsAssigned() {
        this.tournamentService.removeAllTournamentsAssigned(this.member).subscribe(res => {
            this.refreshVue(res);
            
        
        });
    }

    private verificationDateIscription(): boolean {
        let dateNow = new Date();
        let dateFinish = new Date(this.selectedTournamentNotAssigned.finish);
        dateNow.setHours(0, 0, 0, 0);
        dateFinish.setHours(0, 0, 0, 0);
        return dateFinish >= dateNow;
    }
    private placeRestante(): number {
        let maxPlayer = this.selectedTournamentNotAssigned.maxPlayers as number;
        let nbrAssigned = this.selectedTournamentNotAssigned.members.length;
        return maxPlayer - nbrAssigned;

    }

    private refreshVue(res: any) {
        if (this.tournamentsNotAssigned && this.tournamentsAssigned) {
            this.member = res as Member;
            this.tournamentsNotAssigned.refresh();
            this.tournamentsAssigned.refresh();
            this.games.refresh();
            this.message = '';
           
        }
    }

    editProfil() {
        this.editMember.show(this.member).subscribe(res => {
            let updated = _.merge({}, this.member, res.data);
            this.memberService.update(updated).subscribe(res => this.member=res);
        })
    }

}




