import { Component, OnInit, Inject, ElementRef, ViewChild, Output, EventEmitter, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { GameService, Game } from "app/game.service";
import { IDialog, DialogResult } from "app/dialog";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import { MyInputComponent } from "app/myinput.component";
import { MyModalComponent } from "app/mymodal.component";
import { validateConfig } from '@angular/router/src/config';
import { AuthService } from 'app/auth.service';

declare var $: any;

@Component({
    selector: 'edit-game',
    templateUrl: 'edit-game.component.html'
})
export class EditGameComponent implements OnInit, IDialog {
    public frm: FormGroup;
    public ctlScoreOne: FormControl;
    public ctlScoreTwoo: FormControl;
    public ctlPlayerOne: FormControl;
    public ctlPlayerTwoo: FormControl;
    public ctlTournament: FormControl;
    public closed: Subject<DialogResult>;

    @ViewChild(MyModalComponent) modal: MyModalComponent;
    @ViewChild('scoreOne') scoreOne: MyInputComponent;
    @ViewChild('scoreTwoo') ScoreTwoo: MyInputComponent;

    constructor(private gameService: GameService, private fb: FormBuilder, private authService: AuthService) {
        this.ctlScoreOne = this.fb.control('', [Validators.required, Validators.maxLength(3)]);
        this.ctlScoreTwoo = this.fb.control('', [Validators.required, Validators.maxLength(3)]);
        this.ctlPlayerOne = this.fb.control('', []);
        this.ctlPlayerTwoo = this.fb.control('', []);
        this.ctlTournament = this.fb.control('', [])
        this.frm = this.fb.group({
            _id: null,
            scoreOne: this.ctlScoreOne,
            scoreTwoo: this.ctlScoreTwoo,
            playerOne: this.ctlPlayerOne,
            playerTwoo: this.ctlPlayerTwoo,
            tournament: this.ctlTournament,
        }, { validator: this.crossValidations });
    }

    // Validateur bidon qui vérifie que la valeur est différente
    forbiddenValue(val: string): any {
        return (ctl: FormControl) => {
            if (ctl.value === val)
                return { forbiddenValue: { currentValue: ctl.value, forbiddenValue: val } }
            return null;
        };
    }


    static assert(group: FormGroup, ctlName: string[], value: boolean, error: object) {
        ctlName.forEach(n => {
            if (group.contains(n)) {
                if (!value) {
                    group.get(n).setErrors(error);
                    group.get(n).markAsDirty();
                }
                else {
                    group.get(n).setErrors(null);
                }
            }
        });
    }

    crossValidations(group: FormGroup) {
        if (group.pristine || !group.value) return;
        let a = +group.value.scoreOne;
        let b = +group.value.scoreTwoo;
        let c = group.value.scoreOne + "";
        let d = group.value.scoreTwoo + "";
        EditGameComponent.assert(
            group,
            ['scoreOne', 'scoreTwoo'],
            a + b == 1 ||((c ==''||c=='null')&& (d == '' || d == 'null' || d=='0'||d=='0.5'||d=='1'))||((d == ''||d=='null') && (c== '' || c == 'null'|| c=='0'||c=='0.5'||c=='1')),
            { erroScore: true }
        );
    }


    ngOnInit() {
        this.modal.shown.subscribe(_ => {
            this.ctlPlayerOne.value == this.authService.currentUser ?
                this.scoreOne.setFocus(true) :
                this.ScoreTwoo.setFocus(true);
        });
    }

    show(g: Game): Subject<DialogResult> {
        this.closed = new Subject<DialogResult>();
        this.frm.reset();
        this.frm.markAsPristine();
        this.frm.patchValue(g);
        this.modal.show();
        return this.closed;
    }

    update() {
        this.modal.close();
        this.closed.next({ action: 'update', data: this.frm.value });
    }

    cancel() {
        this.modal.close();
        this.closed.next({ action: 'cancel', data: this.frm.value });
    }

    ouvrir() {
        console.log("ouvert");
    }

    fermer() {
        console.log("fermé");
    }
}