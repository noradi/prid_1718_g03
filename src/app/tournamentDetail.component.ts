import { Component, ViewChild } from "@angular/core";
import { MemberService, Member } from "app/member.service";
import { TournamentService, Tournament } from "app/tournament.service";
import { ColumnDef, MyTableComponent } from "app/mytable.component";
import { SnackBarComponent } from "app/snackbar.component";
import { Observable } from "rxjs/Observable";
import { GameService } from "app/game.service";
import { ActivatedRoute } from "@angular/router";
import { OnInit } from "@angular/core/src/metadata/lifecycle_hooks";


@Component({
    selector: 'tournamentDetail',
    templateUrl: './tournamentDetail.component.html',
    styleUrls: ['tournamentDetail.component.css']
})
export class TournamentDetailComponent {


    tournament: Tournament;
    tournamentName: string;
    score: any[]=[];
    @ViewChild('games') games: MyTableComponent;

    columnDefsGames: ColumnDef[] = [
        { name: 'playerOne', type: 'string', header: 'playerOne', width: 1, filter: true, sort: 'asc' },
        { name: 'scoreOne', type: 'number', header: 'score', width: 1, filter: true },
        { name: 'tournament', type: 'string', header: 'tournament', width: 1, filter: true, align: 'center' },
        { name: 'scoreTwoo', type: 'number', header: 'scoreTwoo', width: 1, filter: true, align: 'center' },
        { name: 'playerTwoo', type: 'string', header: 'playerTwoo', width: 1, filter: true, align: 'center' },
    ];

    constructor(private tournamentService: TournamentService, private gameService: GameService, route: ActivatedRoute, private memberService: MemberService) {
        this.tournamentName = route.snapshot.params['id'];
        this.tournamentService.getOne(this.tournamentName).subscribe(res => { this.tournament = res; this.tournamentScore(res) });
    }


    get getDataGameService() {

        return g => this.gameService.getGamesOfTournament(this.tournamentName);
    }

    private tournamentScore(t: Tournament) {
        for (let m of t.members) {
            this.gameService.getScore(m.pseudo, this.tournament).subscribe(res => { this.score.push({ 'pseudo': m.pseudo, 'resultat': res }) })
        }
    }



}
