import { Injectable } from "@angular/core";

@Injectable()
export class ValidationService {
    public getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
        let config = {
            required: 'This field is required',
            minlength: `Minimum length is ${validatorValue.requiredLength}`,
            maxlength: `Maximum length is ${validatorValue.requiredLength}`,
            forbiddenValue: 'This value is forbidden',
            pseudoUsed: 'This pseudo is not available',
            passwordEqualProfile: 'Your password cannot be equal to your profile string',
            erroScore:'your score is not available',
            startInferiorFinish:' finish is not inferior at start',
            passwordNotConfirmed: 'The two passwords must be the same',
            maxPlyerError:'this number is less than the number of members in this tournament',
        };
        return config.hasOwnProperty(validatorName) ? config[validatorName] : '[' + validatorName + ']';
    }
}
