import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { Http, RequestOptions } from "@angular/http";

import 'rxjs/add/operator/map';
import { SecuredHttp } from 'app/securedhttp.service';
import { Tournament } from 'app/tournament.service';
import { AuthHttp } from 'angular2-jwt';
import { Member } from 'app/member.service';

export class Game {
    _id: string;
    playerOne: string;
    scoreOne: number;
    playerTwoo: string;
    scoreTwoo: number;
    tournament: string;

    constructor(data) {
        this._id = data._id;
        this.playerOne = data.playerOne;
        this.scoreOne = data.scoreOne;
        this.playerTwoo = data.playerTwoo;
        this.scoreTwoo = data.scoreTwoo;
        this.tournament = data.tournament;

    }

}

const URL = '/api/games/';

@Injectable()
export class GameService {
    constructor(private http: SecuredHttp) {
    }

    public getAll(): Observable<Game[]> {
        return this.http.get(URL)
            .map(result => {
                let tmp: Game[] = [];
                for (let o of result.json())
                    tmp.push(new Game(o));
                return tmp;
            })
    }
    public getGamesOfTournament(name: string): Observable<Game[]> {
        return this.http.get(URL + name)
            .map(result => {
                let tmp: Game[] = [];
                for (let o of result.json()) {
                    tmp.push(new Game(o));
                }

                return tmp;
            })

    }
    public getGame(m: Member): Observable<Game[]> {
        return this.http.post(URL, m)
            .map(result => {
                let tmp: Game[] = [];
                for (let o of result.json()) {
                    tmp.push(new Game(o));
                }


                return tmp;
            })

    }
    public update(g: Game): Observable<boolean> {
        return this.http.put(URL, g).map(res => true);
    }
   
    public getScore(pseudo: string, t: Tournament): Observable<number> {
        return this.http.post(URL + 'getAllScoreOfMember/' + pseudo, t).map(res => { return res.json() });
    }
}