import { Component, ViewChild } from '@angular/core';
import { MyModalComponent } from "app/mymodal.component";
import { Subject } from "rxjs/Subject";
import { IDialog, DialogResult } from "app/dialog";

@Component({
    selector: 'message',
    template: `
        <mymodal title="Deletion">
            <div body>
                <div> {{message}} </div>
            </div>
            <div footer>
                <button class="btn btn-default" (click)="cancel()">No</button>
            </div>
        </mymodal>
    `
})

export class ConfirmDelete implements IDialog {
    @ViewChild(MyModalComponent) modal: MyModalComponent;
    public closed: Subject<DialogResult>;
    message;

    show(obj?: any): Subject<DialogResult> {
        this.modal.show();
        this.closed = new Subject<DialogResult>();
        return this.closed;
    }

    cancel(): void {
        this.modal.close();
        this.closed.next({ action: 'cancel', data: undefined });
    }

  
}
