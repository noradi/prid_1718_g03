import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { Http, RequestOptions } from "@angular/http";

import 'rxjs/add/operator/map';
import { SecuredHttp } from 'app/securedhttp.service';
import { Tournament } from 'app/tournament.service';
import { AuthHttp } from 'angular2-jwt';
import { Game } from 'app/game.service';

export class Member {
    _id: string;
    pseudo: string;
    password: string;
    profile: string;
    birthdate:string;
    admin:boolean;
    tournaments:Tournament[];
    games: Game[];

    constructor(data) {
        this._id = data._id;
        this.pseudo = data.pseudo;
        this.password = data.password;
        this.profile = data.profile;
        this.birthdate=data.birthdate &&
            data.birthdate.length >10 ? data.birthdate.substring(0,10) : data.birthdate;
        this.admin= data.admin;
        this.tournaments=data.tournaments;
        this.games=data.games;

    }
}

const URL = '/api/members/';

@Injectable()
export class MemberService {
    constructor(private http: SecuredHttp) {
    }
    public getCount(): Observable<number>{
        return this.http.get(URL+'count')
            .map(res=>{return res.json();
            });
    }

    public getAll(): Observable<Member[]> {
        return this.http.get(URL)
            .map(result => {
                let tmp: Member[] = [];
                for (let o of result.json())
                    tmp.push(new Member(o));
                return tmp;
            })
    }

    public getOne(pseudo: string): Observable<Member> {
        return this.http.get(URL + pseudo)
            .map(result => {
                let data = result.json();
                return data.length > 0 ? new Member(data[0]) : null;
            });
    }

    public update(m: Member): Observable<Member> {
        return this.http.put(URL + m.pseudo, m).map(res => new Member(res.json()));
    }

    public delete(m: Member): Observable<boolean> {
        return this.http.delete(URL + m.pseudo).map(res => true);
    }

    public add(m: Member): Observable<Member> {
        return this.http.post(URL, m).map(res => new Member(res.json()));
    }
    public addAllMembers(t: Tournament): Observable<Tournament> {
        return this.http.post(URL + "addAllMembers", t).map(res => new Tournament(res.json()));

    }
    public addOneMember(t: Tournament, m: Member): Observable<Tournament> {
        return this.http.put(URL + 'addOneMember/' + t.name, m).map(res => new Tournament(res.json()));

    }
    public removeMemberAssigned(t: Tournament, m: Member): Observable<Tournament> {
        return this.http.delete(URL + 'removeOneMember/' + t.name + "/" + m.pseudo).map(res => new Tournament(res.json()));
    }
    public removeAllMembersAssigned(t: Tournament): Observable<Tournament> {
        return this.http.post(URL + 'removeAllOneMembers/' + t.name, t).map(res => new Tournament(res.json()));
    }

    public getNbrNotAssigned(t: Tournament): Observable<number> {
        return this.http.post(URL + 'getNbrNotAssigned', t).map(res => res.json());
    }
    public getMembersNotAssigned(t: Tournament): Observable<Member[]> {
        return this.http.post(URL + 'getMembers', t)
            .map(result => {
                let tmp: Member[] = [];
                for (let o of result.json()) {
                    tmp.push(new Member(o));
                }
                return tmp;
            })
    }
    public getMemberAssigned(t: Tournament): Observable<Member[]> {
        return this.http.post(URL + 'getMemberAssigned/', t).map(result => {
            let tmp: Member[] = [];
            for (let o of result.json()) {
                tmp.push(new Member(o));
            }
            return tmp;
        })
    }
   
}
