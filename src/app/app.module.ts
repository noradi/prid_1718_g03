import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, RequestOptions, Http } from "@angular/http";
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TournamentService } from "app/tournament.service";
import { TournamentListComponent } from "app/tournamentlist.component";
import { MemberListComponent } from "app/Memberlist.component";
import { LoginComponent } from "app/login.component";
import { HomeComponent } from "app/home.component";
import { UnknownComponent } from "app/unknown.component";
import { MemberService } from 'app/member.service';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { AuthGuard, AdminGuard } from 'app/auth-guard.service';
import { RestrictedComponent } from 'app/restricted.component';
import { AuthService } from 'app/auth.service';
import { SecuredHttp } from 'app/securedhttp.service';
import { MyInputComponent } from 'app/myinput.component';
import { ValidationService } from 'app/validation.service';
import { MyTableComponent } from 'app/mytable.component';
import { ConfirmDelete } from 'app/confirm-delete.component';
import { EditMemberComponent } from 'app/edit-member.component';
import { SnackBarComponent } from 'app/snackbar.component';
import { LogoutComponent } from 'app/logout.component';
import { SignUpComponent } from 'app/signup.component';
import { EditTournamentComponent } from 'app/edit-tournament.component';
import { MembersCommonRouter } from '../../server/routes/members-common.router';
import { EspaceMember } from 'app/espaceMember.component';
import { GameService } from 'app/game.service';
import { EditGameComponent } from 'app/edit-game.component';
import { TournamentDetailComponent } from 'app/tournamentDetail.component';
import { MyModalComponent } from 'app/mymodal.component';




export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(
      new AuthConfig({
          tokenGetter: (() => sessionStorage.getItem('id_token'))
      }),
      http,
      options
  );
}


@NgModule({
  declarations: [
    AppComponent,
    TournamentListComponent,
    TournamentDetailComponent,
    MemberListComponent,
    EspaceMember,
    LoginComponent,
    HomeComponent,
    SignUpComponent,
    UnknownComponent,
    RestrictedComponent,
    ConfirmDelete,
    MyInputComponent,
    MyTableComponent,
    MyModalComponent,
    EditMemberComponent,
    EditTournamentComponent,
    EditGameComponent,
    SnackBarComponent,
    LogoutComponent,
    
    
    
    
    
    

  
   
  ],
  imports: [
    HttpModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path:'signup', component: SignUpComponent},
      { 
        path:'',
        canActivate: [AuthGuard],
        children :[

            { path: 'logout', component: LogoutComponent },
            { path: 'home', component: HomeComponent },
            {path: 'espaceMember', component: EspaceMember},
            {
              path:'',
              canActivate:[AdminGuard],
              children:[
                { path: 'members', component: MemberListComponent },
                { path: 'tournaments', component: TournamentListComponent },
                { path: 'tournamentDetail/:id', component: TournamentDetailComponent },
              
              ]
            },
     
        ]
      },

      { path:'restricted', component:RestrictedComponent},
      { path: '**', component: UnknownComponent }
  ])
  ],
    providers: [
      {
          provide: AuthHttp,
          useFactory: authHttpServiceFactory,
          deps: [Http, RequestOptions]
      },
      SecuredHttp,
      AuthGuard,
      AdminGuard,
      AuthService,
      MemberService,
      TournamentService,
      GameService,
      ValidationService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
