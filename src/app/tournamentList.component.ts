import { Component, ViewChild } from "@angular/core";
import { MemberService, Member } from "app/member.service";
import { TournamentService, Tournament } from "app/tournament.service";
import { EditTournamentComponent } from "app/edit-tournament.component";
import { ColumnDef, MyTableComponent } from "app/mytable.component";
import { SnackBarComponent } from "app/snackbar.component";
import { Observable } from "rxjs/Observable";
import { DatePipe } from '@angular/common';
import * as moment from 'moment'

@Component({
    selector: 'tournamentlist',
    templateUrl: './tournamentlist.component.html',
    styleUrls:['./tournamentlist.component.css'],
})
export class TournamentListComponent {
    selectedTournament: Tournament;
    selectedMemberNotAssigned: Member;
    selectedMemberAssigned: Member;
    message: string;
    nbrNotAssigend: number;

    @ViewChild('tournaments') tournaments: MyTableComponent;
    @ViewChild('membersNotAssigned') membersNotAssigned: MyTableComponent;
    @ViewChild('membersAssigned') membersAssigned: MyTableComponent;


    columnDefs: ColumnDef[] = [
        { name: 'name', type: 'String', header: 'Name', width: 1, key: true, filter: true, sort: 'asc' },
        { name: 'start', type: 'Date', header: 'Start', width: 2, filter: true },
        { name: 'finish', type: 'Date', header: 'finish', width: 1, filter: true, align: 'center' },
        { name: 'maxPlayers', type: 'Number', header: 'Max Players', width: 1, filter: false, align: 'center' }
    ];
    columnDefsMembersNotAssigned: ColumnDef[] = [
        { name: 'pseudo', type: 'String', header: 'Pseudo', width: 1, filter: true, sort: 'asc' },
        { name: 'profile', type: 'String', header: 'Profile', width: 2, filter: true },
        { name: 'birthdate', type: 'Date', header: 'Birth Date', width: 1, filter: true, align: 'center' },
    ];
    columnDefsMembersAssigned: ColumnDef[] = [
        { name: 'pseudo', type: 'String', header: 'Pseudo', width: 1, filter: true, sort: 'asc' },
        { name: 'profile', type: 'String', header: 'Profile', width: 2, filter: true },
        { name: 'birthdate', type: 'Date', header: 'Birth Date', width: 1, filter: true, align: 'center' },
    ];

    constructor(private tournamentService: TournamentService, private memberService: MemberService) {
    }

    get getDataService() {
        return m => this.tournamentService.getAll();
    }

    get addService() {
        return m => this.tournamentService.add(m);
    }

    get deleteService() {
        return m => this.tournamentService.delete(m);
    }

    get updateService() {

        return m => this.tournamentService.update(m).subscribe(res => this.selectedTournament = res);
    }

    public selectedItemChanged(item) {
        this.selectedTournament = this.tournaments.selectedItem as Tournament;
        this.memberService.getNbrNotAssigned(this.selectedTournament).subscribe(res => {this.nbrNotAssigend = res});
        if (this.membersNotAssigned && this.membersAssigned) {
            this.membersNotAssigned.refresh();
            this.membersAssigned.refresh();
            this.message = '';
        }
    }
    public selectedItemMemberNotAssigned(item) {
        this.selectedMemberNotAssigned = this.membersNotAssigned.selectedItem as Member;
        this.message='';
    }
    public selectedItemMemberAssigned(item) {
        this.selectedMemberAssigned = this.membersAssigned.selectedItem as Member;
        this.message='';

    }
    get getDataMembersNotAssignedService() {

        return m => this.memberService.getMembersNotAssigned(this.selectedTournament);
    }
    get getDataMembersAssignedService() {

        return m => this.memberService.getMemberAssigned(this.selectedTournament);
    }


    addAllMembersNotAssigned() {
        if(this.selectedTournament){
            if (this.verificationDateIscription()) {
                if (this.placeRestante() >= this.nbrNotAssigend) {
                    this.memberService.addAllMembers(this.selectedTournament).subscribe(res => this.refreshVue(res))
                }
                else
                    this.message = "le nombre de joeurs est limité à " + this.selectedTournament.maxPlayers
            }
            else
                this.message = "la date du tournoi est expirèe !, veuillez choisir un autre tournoi";
        }
        else
        this.message='veuillez selectionner un tournoi !!!'
       

    }

    addMemberNotAssigned() {
        if (this.selectedItemMemberNotAssigned && this.selectedTournament) {
            if (this.verificationDateIscription()) {
                if (this.placeRestante() >= 1) {
                    this.memberService.addOneMember(this.selectedTournament, this.selectedMemberNotAssigned)
                        .subscribe(res => this.refreshVue(res));
                }
                else
                    this.message = "le nombre de joeurs est limité à " + this.selectedTournament.maxPlayers
            }
            else
                this.message = "la date est expirée veuillez choisir un autre tournoi !"

        }
        else
            this.message = "veillez selectionner un member et un tournoi  !!"

    }
    removeMemberAssigned() {
        if (this.selectedMemberAssigned) {
            this.memberService.removeMemberAssigned(this.selectedTournament, this.selectedMemberAssigned)
                .subscribe(res => this.refreshVue(res))
        }
    }
    removeAllMembersAssigned() {
        this.memberService.removeAllMembersAssigned(this.selectedTournament).subscribe(res => this.refreshVue(res));
    }

    private verificationDateIscription(): boolean {
        let dateNow = new Date();
        let dateFinish = new Date(this.selectedTournament.finish);
        dateNow.setHours(0, 0, 0, 0);
        dateFinish.setHours(0, 0, 0, 0);
        return dateFinish >= dateNow;
    }
    private placeRestante(): number {
        let maxPlayer = this.selectedTournament.maxPlayers as number;
        let nbrAssigned = this.selectedTournament.members.length;
        return maxPlayer - nbrAssigned;
    }

    private refreshVue(res: any) {
        if (this.membersNotAssigned && this.membersAssigned) {
            this.selectedTournament = res as Tournament;
            this.membersNotAssigned.refresh();
            this.membersAssigned.refresh();
            this.tournaments.refresh();
            this.message = '';

        }
    }




}