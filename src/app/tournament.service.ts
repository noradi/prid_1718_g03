import { Member } from './member.service';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { Http, RequestOptions } from "@angular/http";
import { SecuredHttp } from "app/securedhttp.service";

import 'rxjs/add/operator/map';
import { Game } from 'app/game.service';

export class Tournament {
    _id: string;
    name: string;
    start: Date;
    finish: Date;
    maxPlayers: Number
    members: Member[]
    games: Game [];

    constructor(data) {
        this._id = data._id;
        this.name = data.name;
        this.start = data.start &&
            data.start.length > 10 ? data.start.substring(0, 10) : data.start;
        this.finish = data.finish &&
            data.finish.length > 10 ? data.finish.substring(0, 10) : data.finish;
        this.maxPlayers = data.maxPlayers;
        this.members = data.members;
        this.games=data.games;
    }
}

const URL = '/api/tournaments/';

@Injectable()
export class TournamentService {
    constructor(private http: SecuredHttp) {

    }
    public getAll(): Observable<Tournament[]> {
        return this.http.get(URL)
            .map(result => {
                return result.json().map(json => new Tournament(json));
            });
    }
    public getOne(name: string): Observable<Tournament> {
        return this.http.get(URL + name)
            .map(result => {
                let data = result.json();
                return data.length > 0 ? new Tournament(data[0]) : null;
            });
    }
    public update(t: Tournament): Observable<Tournament> {
        return this.http.put(URL + t.name, t).map(res => new Tournament(res.json));
    }


    public delete(t: Tournament): Observable<boolean> {
        return this.http.delete(URL + t.name).map(res => true);
    }

    public add(t: Tournament): Observable<Tournament> {
        return this.http.post(URL, t).map(res => new Tournament(res.json()));
    }
    public addAlltournaments(m: Member): Observable<Member> {

        return this.http.post(URL + "addAllTournaments", m).map(res => new Member(res.json()));

    }
    public addOneTournament( m: Member,t: Tournament): Observable<Member> {
        return this.http.put(URL + 'addOneTournament/' + m.pseudo, t).map(res => new Member(res.json()));

    }
    public removeAllTournamentsAssigned(m: Member): Observable<Member> {
        return this.http.post(URL + 'removeAllTournaments', m).map(res => new Member(res.json()));
    }
    public removeTournamentAssigned( m: Member,t: Tournament): Observable<Member> {
        return this.http.delete(URL + 'removeOneTournament/' + t.name + "/" + m.pseudo).map(res => new Member(res.json()));
    }
   
    public getTournamentsNotAssigned(m: Member): Observable<Tournament[]> {
        
        return this.http.post(URL + 'getTournamentsNotAssigned',m).map(result => {
            let tmp: Tournament[] = [];
            for (let o of result.json()) {
                tmp.push(new Tournament(o));
            }
            return tmp;
        })
    }
    public getTournamentsAssigned(m: Member): Observable<Tournament[]> {
        return this.http.post(URL + 'getTournamentsAssigned', m).map(result => {
            let tmp: Tournament[] = [];
            for (let o of result.json()) {
                tmp.push(new Tournament(o));
            }
            return tmp;
        })
    }


}