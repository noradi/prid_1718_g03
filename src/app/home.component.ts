import { Component, ViewChild, OnInit } from '@angular/core';
import { AuthService } from 'app/auth.service';
import { MemberService, Member } from 'app/member.service';
import { EditMemberComponent } from "app/edit-member.component";
import { MyTableComponent, ColumnDef } from 'app/mytable.component';
import { TournamentService,Tournament } from 'app/tournament.service';
import { GameService } from 'app/game.service';

@Component({
    selector: 'app-root',
    templateUrl: 'home.component.html'
})
export class HomeComponent  {

    memberCount: number | '?' ='?';
   

    constructor(public authService : AuthService, private memberService: MemberService, private gameService: GameService){
        this.memberService.getCount().subscribe(count=> this.memberCount=count);  
    }
   
   
}
