import { Member } from './../models/member';
import { Router, Request, Response, NextFunction } from 'express';
import * as mongoose from 'mongoose';
import { Tournament } from '../models/tournament';
import { Game } from '../models/game';



export class TournamentsRouter {
    public router: Router;

    constructor() {
        this.router = Router();

        this.router.get('/', this.getAll);
        this.router.post('/', this.create);
        this.router.get('/:name', this.findByName);
        this.router.get('/byId/:id', this.findById);
        this.router.get('/byStartDate/:start', this.findByStartDate);
        this.router.get('/byFinishDate/:finish', this.findByFinishDate);
        this.router.get('/byMaxPlayers/:max', this.findByMaxPlayers);
        this.router.get('/byStartRange/:start/:finish', this.getRange);
        this.router.put('/:name', this.update);
        this.router.delete('/:name', this.deleteOne);
        this.router.delete('/:start/:finish', this.deleteRange);
        this.router.post('/getTournamentsNotAssigned', this.getTournamentsNotAssigned);
        this.router.post('/getTournamentsAssigned', this.getTournamentsAssigned);
        this.router.post('/addAllTournaments', this.addAllTournaments);
        this.router.put('/addOneTournament/:pseudo', this.addOneTournament);
        this.router.post('/removeAllTournaments', this.removeAllTournaments);
        this.router.delete('/removeOneTournament/:name/:pseudo', this.removeOneTournament);



    }

    public removeAllTournaments(req: Request, res: Response, next: NextFunction) {
        let m = new Member(req.body).populate('tournaments');
        m.isNew = false;
        Tournament.find({ _id: { $in: m.tournaments } })
            .then(ts => ts.forEach(t => {
                t.members.remove(m);
                m.tournaments.remove(t);
                t.save()

            }))
            .then(() => {
                Game.remove({ $or: [{ playerOne: m.pseudo }, { playerTwoo: m.pseudo }] })
                    .then(res => console.log(true))
                m.save();
                res.json(m.populate('tournaments'))
            })
            .catch(err => res.json(err));

    }
    public addAllTournaments(req: Request, res: Response, next: NextFunction) {
        let dateNow = new Date();
        dateNow.setHours(0, 0, 0, 0);
        let m = new Member(req.body).populate('tournaments');
        m.isNew = false;
        Tournament.find({ _id: { $nin: m.tournaments } }).populate('members')
            .then(ts => ts.forEach(t => {
                let dateFinish = new Date(t.finish);
                dateFinish.setHours(0, 0, 0, 0);
                if (dateFinish >= dateNow && t.maxPlayers > t.members.length) {
                    t.members.push(m);
                    t.save()
                    m.tournaments.push(t);
                    m.save();
                    t.members.forEach(mm => {
                        if (m._id != mm._id) {
                            Member.findOne({ _id: mm._id })
                                .then(m2 => {
                                    Game.insertMany([{ tournament: t.name, playerOne: m.pseudo, scoreOne: "", playerTwoo: m2.pseudo, scoreTwoo: "" }])
                                        .then(res => console.log('game create'))
                                })
                        }
                    })
                }
            })
            )
            .then(() => { res.json(m) })
            .catch(err => res.json(err));
    }
    public addOneTournament(req: Request, res: Response, next: NextFunction) {
        let tournament = new Tournament(req.body);
        tournament.isNew = false;
        let pseudo = req.params.pseudo;
        Member.findOne({ pseudo: pseudo }).populate('tournaments')
            .then(m => {
                if (!m.tournaments.find(t => t._id == tournament.id)) {
                    m.tournaments.push(tournament);
                    m.save()
                        .then(() => {
                            tournament.members.push(m);
                            tournament.save()
                                .then(() => {
                                    tournament.members.forEach(mt => {
                                        if (mt != m._id) {
                                            Member.findOne({ _id: mt })
                                                .then(m2 => {
                                                    Game.insertMany([{ tournament: tournament.name, playerOne: m.pseudo, scoreOne: "", playerTwoo: m2.pseudo, scoreTwoo: "" }])
                                                        .then(games => { console.log(true) })
                                                })

                                        }
                                    })
                                })
                        })
                }

                return m;
            })
            .then(m => res.json(m))
            .catch(err => res.json(err));
    }
    public removeOneTournament(req: Request, res: Response, next: NextFunction) {
        let name = req.params.name
        let pseudo = req.params.pseudo
        Member.findOne({ pseudo: pseudo }).populate('tournaments')
            .then(m => {
                let t = m.tournaments.find(tr => tr.name == name)
                m.tournaments.remove(t);
                m.save()
                    .then(m => {
                        t.members.remove(m);
                        t.save()
                            .then(t => {
                                Game.remove({ $and: [{ tournament: t.name }, { $or: [{ playerOne: m.pseudo }, { playerTwoo: m.pseudo }] }] })
                                    .then(res => console.log(true))
                            })
                    })
                return m;
            })


            .then(m => res.json(m))
            .catch(err => console.log(err))

    }


    public getTournamentsNotAssigned(req: Request, res: Response, next: NextFunction) {
        let m = new Member(req.body);
        m.isNew = false;
        let ids = m.tournaments;
        Tournament.find({ _id: { $nin: ids } }).populate('members')
            .then(tournaments => {
                res.json(tournaments)
            })
            .catch(err => res.json([]))
    }
    public getTournamentsAssigned(req: Request, res: Response, next: NextFunction) {
        let m = new Member(req.body)
        m.isNew = false;
        let ids = m.tournaments;
        Tournament.find({ _id: { $in: ids } }).populate('members')
            .then(tournaments => res.json(tournaments))
            .catch(err => res.json([]))
    }



    public getAll(req: Request, res: Response, next: NextFunction) {
        Tournament.find().sort({ name: 'asc' }).populate('members')
            .then(tournaments => res.json(tournaments))
            .catch(err => res.json([]));
    }

    public findById(req: Request, res: Response, next: NextFunction) {
        Tournament.find({ _id: req.params.id }).populate('members')
            .then(tournament => res.json(tournament))
            .catch(err => res.json([]));
    }


    public findByName(req: Request, res: Response, next: NextFunction) {
        Tournament.find({ name: req.params.name }).populate('members')
            .then(tournament => res.json(tournament))
            .catch(err => res.json([]));
    }


    public findByStartDate(req: Request, res: Response, next: NextFunction) {
        let d = new Date(req.params.start);
        if (!isNaN(d.valueOf())) {
            Tournament.find({ start: d }).sort({ name: 'asc' }).populate('members')
                .then(tournaments => res.json(tournaments))
                .catch(err => res.json([]));
        }
        else
            res.json([]);
    }

    public findByFinishDate(req: Request, res: Response, next: NextFunction) {
        let d = new Date(req.params.finish);
        if (!isNaN(d.valueOf())) {
            Tournament.find({ finish: d }).sort({ name: 'asc' }).populate('members')
                .then(tournaments => res.json(tournaments))
                .catch(err => res.json([]));
        }
        else
            res.json([]);
    }

    public findByMaxPlayers(req: Request, res: Response, next: NextFunction) {
        Tournament.find({ maxPlayers: req.params.max }).sort({ name: 'asc' }).populate('members')
            .then(tournaments => res.json(tournaments))
            .catch(err => res.json([]));
    }

    public getRange(req: Request, res: Response, next: NextFunction) {
        let d1 = new Date(req.params.start);
        let d2 = new Date(req.params.finish);
        if (isNaN(d1.valueOf()) || isNaN(d2.valueOf()))
            res.json({ errmsg: 'bad date range' });
        else {
            Tournament.find({ start: { $gte: d1, $lte: d2 } }).populate('members')
                .then(tournaments => res.json(tournaments))
                .catch(err => res.json([]));
        }
    }

    public create(req: Request, res: Response, next: NextFunction) {
        let tournament = new Tournament(req.body);
        tournament.save()
            .then(r => res.json(r))
            .catch(err => res.json(err));
    }

    public update(req: Request, res: Response, next: NextFunction) {
        Tournament.findOneAndUpdate({ name: req.params.name },
            req.body,
            { new: true })  // pour renvoyer le document modifié
            .then(r => res.json(r))
            .catch(err => res.json(err));
    }

    public deleteOne(req: Request, res: Response, next: NextFunction) {
        Tournament.findOneAndRemove({ name: req.params.name }).populate('members')
            .then(t => {
                t.members.forEach(m => {
                    m.tournaments.remove(t);
                    m.save()
                        .then(() => {
                            Game.remove({ tournament: t.name })
                                .then(res => console.log(true))
                        })
                })
            })
            .then(() => res.json(true))
            .catch(err => res.json(err));
    }

    public deleteRange(req: Request, res: Response, next: NextFunction) {
        let d1 = new Date(req.params.start);
        let d2 = new Date(req.params.finish);
        if (isNaN(d1.valueOf()) || isNaN(d2.valueOf()))
            res.json({ errmsg: 'bad date range' });
        else {
            Tournament.find({ start: { $gte: d1, $lte: d2 } })
                .remove()
                .then(r => res.json(true))
                .catch(err => res.json(err));
        }
    }


}