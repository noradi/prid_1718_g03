import { Member } from './../models/member';
import { Router, Request, Response, NextFunction } from 'express';
import * as mongoose from 'mongoose';
import { Tournament } from '../models/tournament';
import { Game } from '../models/game';
import { forEach } from '@angular/router/src/utils/collection';

export class GamesRouter {

    public router: Router;

    constructor() {
        this.router = Router();

        this.router.get('/', this.getAll);
        this.router.post('/', this.getGame);
        this.router.get('/:name', this.getGamesOfTournament);
        this.router.post('/getAllScoreOfMember/:pseudo', this.getAllScoreOfMember)
        this.router.put('/', this.update);
    }


    public getAllScoreOfMember(req: Request, res: Response, next: NextFunction) {
        let t = new Tournament(req.body);
        let pseudo = req.params.pseudo;
        let result = 0;
        Game.find({ $and: [{ tournament: t.name }, { $or: [{ playerOne: pseudo }, { playerTwoo: pseudo }] }] })
            .then(games => {
                games.forEach(g => {
                    let score = g.playerOne == pseudo ? g.scoreOne : g.scoreTwoo;
                    if (score == null) {
                        let scoreAdversaire = g.playerOne != pseudo ? g.scoreOne : g.scoreTwoo;
                        if (scoreAdversaire != null) {
                            if (scoreAdversaire == 1)
                                result = result
                            else if (scoreAdversaire == 0.5)
                                result = result + 0.5
                            else
                                result = result + 1
                        }
                        else
                            result = result;
                    }
                    else
                        result = result + score
                })
                return result;
            })
            .then(result => res.json(result))

            .catch(err => res.json());
    }
    public getAll(req: Request, res: Response, next: NextFunction) {
        Game.find()
            .then(games => res.json(games))
            .catch(err => res.json([]));
    }
    public getGamesOfTournament(req: Request, res: Response, next: NextFunction) {
        Game.find({ tournament: req.params.name })
            .then(games => res.json(games))
            .catch(err => res.json([]));

    }
    public getGame(req: Request, res: Response, next: NextFunction) {
        let m = new Member(req.body);
        m.isNew = false;
        let name = req.params.name;
        Game.find({ $or: [{ playerOne: m.pseudo }, { playerTwoo: m.pseudo }] })
            .then(games => res.json(games))
            .catch(err => res.json([]));

    }
    public update(req: Request, res: Response, next: NextFunction) {
        let game = new Game(req.body);
        Game.findOneAndUpdate({ _id: game._id },
            req.body,
            { new: true },  // pour renvoyer le document modifié
            function (err, task) {
                if (err)
                    res.send(err);
                res.json(task);
            });
    }

}
