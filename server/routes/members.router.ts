import { Router, Request, Response, NextFunction } from 'express';
import * as mongoose from 'mongoose';
import Member from '../models/member';
import { AuthentificationRouter } from './authentification.router';
import { Tournament } from '../models/tournament';
import { Game } from '../models/game';


export class MembersRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.router.get('/count', this.getCount);
        this.router.get('/:id', this.getOne);
        this.router.post('/getMembers', this.getAllMembersNotAssigned);
        this.router.post('/getMemberAssigned', this.getMemberAssigned);
        this.router.post('/getNbrNotAssigned', this.nbrNotAssigned);
        this.router.put('/addOneMember/:name', this.addOneMember);
        this.router.delete('/removeOneMember/:name/:pseudo', this.removeOneMember);
        this.router.post('/removeAllOneMembers/:name', this.removeAllMembers);
        this.router.post('/addAllMembers', this.addAllMembers);
        this.router.use(AuthentificationRouter.checkAdmin)
        this.router.get('/', this.getAll);
        this.router.post('/', this.create);
        this.router.delete('/', this.deleteAll);
        this.router.put('/:id', this.update);
        this.router.delete('/:id', this.deleteOne);


    }
    public getAllMembersNotAssigned(req: Request, res: Response, next: NextFunction) {
        let t = new Tournament(req.body)
        t.isNew = false;
        let ids = t.members;
        Member.find({ _id: { $nin: ids } }).populate('tournaments')
            .then(members => res.json(members))
            .catch(err => res.json([]))
    }
    public getMemberAssigned(req: Request, res: Response, next: NextFunction) {
        let t = new Tournament(req.body);
        t.isNew = false;
        Member.find({ _id: { $in: t.members } }).populate('tournaments')
            .then(members => res.json(members))
            .catch(err => res.json(err))
    }
    public nbrNotAssigned(req: Request, res: Response, next: NextFunction) {
        let t = new Tournament(req.body);
        t.isNew = false;
        let ids = t.members.map(m => m._id)
    
        Member.count({ _id: { $nin: ids } })
            .then(n => res.json(n))
            .catch(err => res.json(err));
    }
    public removeAllMembers(req: Request, res: Response, next: NextFunction) {
        let t = new Tournament(req.body).populate('members');
        t.isNew = false;
        Member.find({ _id: { $in: t.members } })
            .then(ms => ms.forEach(m => {
                m.tournaments.remove(t);
                t.members.remove(m);
                m.save()

            }))
            .then(() => {
                Game.remove({ tournament: t.name })
                    .then(res => console.log(true))
                t.save();
                res.json(t.populate('members'))
            })
            .catch(err => res.json(err));

    }
    public addAllMembers(req: Request, res: Response, next: NextFunction) {

        let t = new Tournament(req.body).populate('members');
        t.isNew = false;
        Member.find({ _id: { $nin: t.members } }).populate('tournaments')
            .then(ms => ms.forEach(m => {
                t.members.forEach(mm => {
                    if (m._id != mm) {
                        Member.findOne({ _id: mm })
                            .then(m2 => {
                                Game.insertMany([{ tournament: t.name, playerOne: m.pseudo, scoreOne: "", playerTwoo: m2.pseudo, scoreTwoo: "" }])
                                    .then(res => console.log('game create'))
                            })

                    }
                })
                t.members.push(m);
                t.save()
                    .then(() => {
                        m.tournaments.push(t);
                        m.save()
                    })
            }))

            .then(() => { res.json(t); })
            .catch(err => res.json(err));


    }
    public removeOneMember(req: Request, res: Response, next: NextFunction) {
        let name = req.params.name
        let pseudo = req.params.pseudo
        Tournament.findOne({ name: name }).populate('members')
            .then(t => {
                let m = t.members.find(ms => ms.pseudo == pseudo)
                t.members.remove(m);
                t.save()
                    .then(() => {
                        m.tournaments.remove(t);
                        m.save()
                            .then(() => {
                                Game.remove({ $and: [{ tournament: t.name }, { $or: [{ playerOne: m.pseudo }, { playerTwoo: m.pseudo }] }] })
                                    .then(res => console.log(true))
                            })
                    })
                return t;

            })
            .then(t => res.json(t))
            .catch(err => console.log(err))

    }


    public addOneMember(req: Request, res: Response, next: NextFunction) {
        let member = new Member(req.body);
        member.isNew = false;
        let name = req.params.name;
        Tournament.findOne({ name: name }).populate('members')
            .then(t => {
                if (!t.members.find(m => m._id == member.id)) {
                    t.members.push(member)
                    t.save()
                        .then(() => {
                            member.tournaments.push(t);
                            member.save()
                        })
                        .then(() => {
                            t.members.forEach(m2 => {
                                if (m2._id != member._id) {
                                    Game.insertMany([{ tournament: t.name, playerOne: member.pseudo, scoreOne: "", playerTwoo: m2.pseudo, scoreTwoo: "" }])
                                        .then(games => { console.log(true) })
                                }
                            })
                        })

                }
                return t;
            })


            .then(t => res.json(t))
            .catch(err => res.json(err));
    }

    public getCount(req: Request, res: Response, next: NextFunction) {
        Member.count({}).exec((err, count) => {
            if (err)
                res.send(err);
            res.json(count)
        });
    }

    public getAll(req: Request, res: Response, next: NextFunction) {

        Member.find().sort({ pseudo: 'asc' }).exec((err, members) => {
            if (err) res.send(err);
            res.json(members);
        });
    }


    public getOne(req: Request, res: Response, next: NextFunction) {
        Member.find({ pseudo: req.params.id }, (err, members) => {
            if (err) res.send(err);
            res.json(members);
        }).populate('tournaments').populate('games');
    }

    public create(req: Request, res: Response, next: NextFunction) {
        delete req.body._id;
        let member = new Member(req.body);
        member.save(function (err, task) {
            if (err) res.send(err);
            res.json(task);
        });
    }

    public update(req: Request, res: Response, next: NextFunction) {
        let member = new Member(req.body);
        Member.findOneAndUpdate({ pseudo: req.params.id },
            req.body,
            { new: true },  // pour renvoyer le document modifié
            function (err, task) {
                console.log(err, task);
                if (err)
                    res.send(err);
                res.json(task);
            });
    }

    public deleteOne(req: Request, res: Response, next: NextFunction) {
        Member.findOneAndRemove({ pseudo: req.params.id }).populate('tournaments')
            .then(member => {
                member.tournaments.forEach(t => {
                    t.members.remove(member)
                    t.save()
                        .then(() => {
                            Game.remove({ $or: [{ playerOne: member.pseudo }, { playerTwoo: member.pseudo }] })
                                .then(res => console.log(true))
                        })
                })
                return true;
            })
            .then(r => res.json(true))
            .catch(err => res.json(err))


    }

    public deleteAll(req: Request, res: Response, next: NextFunction) {
        Member.remove({},
            function (err) {
                if (err)
                    res.send(err);
                res.json({ status: 'ok' });
            });
    }
}