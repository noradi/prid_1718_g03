import * as http from 'http';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import Tournament from './models/tournament';
import Member from './models/member';
import { TournamentsRouter } from './routes/tournaments.router';
import { MembersRouter } from './routes/members.router';
import { AuthentificationRouter } from './routes/authentification.router';
import { MembersCommonRouter } from './routes/members-common.router';
import { GamesRouter } from './routes/games.router';

const MONGO_URL = 'mongodb://127.0.0.1/msn';

export class Server {
    private express: express.Application;
    private server: http.Server;
    private port: any;

    constructor() {
        this.express = express();
        this.middleware();
        this.mongoose();
        this.routes();
    }

    private middleware(): void {
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
    }

    // initialise les routes
    private routes() {
        this.express.use('/api/token', new AuthentificationRouter().router);
        this.express.use('/api/members-common/', new MembersCommonRouter().router)
        this.express.use(AuthentificationRouter.checkAuthorization);
        this.express.use('/api/tournaments', new TournamentsRouter().router);
        this.express.use('/api/members', new MembersRouter().router);
        this.express.use('/api/games', new GamesRouter().router);
        
    }

    // initialise mongoose
    private mongoose() {
        (mongoose as any).Promise = global.Promise;     // see: https://stackoverflow.com/a/38833920
        let trials = 0;
        let connectWithRetry = () => {
            trials++;
            return mongoose.connect(MONGO_URL, err => {
                if (err) {
                    if (trials < 3) {
                        console.error('Failed to connect to mongo on startup - retrying in 2 sec');
                        setTimeout(connectWithRetry, 2000);
                    }
                    else {
                        console.error('Failed to connect to mongo after 3 trials ... abort!');
                        process.exit(-1);
                    }
                }
                else {
                    console.log('Connected to MONGODB');
                    this.initDataTournament();
                    this.initDataMember();
                }
            });
        };
        connectWithRetry();
    }

    private initDataTournament() {
       
        Tournament.count({}).then(count => {
            if (count === 0) {
                console.log("Initializing data...");
                Tournament.insertMany([
                    { name: "T_01", start: "09-10-17", finish: "10-10-18" ,maxPlayers: 16 },
                    { name: "T_02", start: "09-10-17", finish: "10-10-18" ,maxPlayers: 16 },
                    { name: "T_03", start: "09-10-17", finish: "10-10-18" ,maxPlayers: 16 },
                    { name: "T_04", start: "09-10-17", finish: "10-10-18" ,maxPlayers: 16 },
                   
                    
                ]);
            }
        });
    }
    private initDataMember() {
        // let col = mongoose.connection.collections['members'];
    
        Member.count({}).then(count => {
            if (count === 0) {
                console.log("Initializing data...");
                Member.insertMany([
                    { pseudo: "ben", password: "ben", profile:"I'am ben ",birthdate:"01-02-2000" ,admin:true},
                    { pseudo: "mimo", password: "mimo", profile:"hi, I am mimo ",birthdate:"01-03-2001",admin:false},
                    { pseudo: "martin", password: "martin", profile:"hi, I am martin ",birthdate:"01-04-2002",admin:false},
                    { pseudo: "noradi", password: "noradi", profile:"hi, I am noradi ",birthdate:"01-05-2003",admin:false },
                   
                    
                ]);
            }
        });
        Member.count({pseudo : 'admin'}).then(count=>{
            if(count===0){
                console.log("creating admin ...");
                let m = new Member({
                    pseudo: "admin", password:"admin",birthdate:"01-06-2004", profile: "I'm the administrator of the site!", admin: true  
                });
                m.save();
            }
        });
    }

    // démarrage du serveur express
    public start(): void {
        this.port = process.env.PORT || 3000;
        this.express.set('port', this.port);
        this.server = http.createServer(this.express);
        this.server.listen(this.port, () => console.log(`Node/Express server running on localhost:${this.port}`));
    }
}
