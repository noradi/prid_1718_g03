import * as mongoose from 'mongoose';
import { ITournament } from './tournament';
import { IGame } from './game';

let Schema = mongoose.Schema;

export interface IMember extends mongoose.Document {
    pseudo: string;
    password: string;
    profile: string;
    birthdate: string;
    admin: boolean;
    tournaments: mongoose.Types.Array<ITournament>;
    
}

let memberSchema = new mongoose.Schema({
    pseudo: { type: String, required: true, unique: true },
    password: { type: String, default: '' },
    profile: { type: String, default: '' },
    birthdate: { type: Date },
    admin: { type: Boolean, default: false },
    tournaments: [{ type: Schema.Types.ObjectId, ref: 'Tournament' }],
    
});

export let Member = mongoose.model<IMember>('Member', memberSchema);

export default Member;