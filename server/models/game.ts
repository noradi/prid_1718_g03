import * as mongoose from 'mongoose';
import { ITournament } from './tournament';
import { IMember } from './member';

let Schema = mongoose.Schema;

export interface IGame extends mongoose.Document{
   tournament:string;
   playerOne:string;
   scoreOne:number;
   playerTwoo:string;
   scoreTwoo:number;  
}

let gameSchema = new mongoose.Schema({
    tournament:String,
    playerOne:String ,
    scoreOne: Number,
    playerTwoo:String,
    scoreTwoo:Number,
});

export let Game = mongoose.model<IGame>('Game', gameSchema);

export default Game;